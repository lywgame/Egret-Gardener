# Egret Gardener

#### Description
Gardener作为一款轻型的白鹭游戏UI框架，提供丰富UI功能，满足绝大部分的游戏产品的需求。兼容EUI，适配横屏和竖屏项目，框架接口清晰简单，控制灵活，是从线上多款产品框架中总结提炼出来，而且完全开源。此框架的意义在于解放开发者，让开发者把更多的精力用在游戏的核心玩法上。当前最低 egret engine 5.0.14 测试通过。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
